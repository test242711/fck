import com.microsoft.playwright.*;
import db_fck.DbConnection;
import org.springframework.jdbc.core.JdbcTemplate;

public class TestBasePW {
    protected Playwright playwright = Playwright.create();
    protected Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
    protected BrowserContext context = browser.newContext();
    protected Page page = context.newPage();

    // Подключение к БД
    DbConnection dbConnection = new DbConnection();
    final JdbcTemplate jdbcTemplate = dbConnection.jdbcTemplate();
}
