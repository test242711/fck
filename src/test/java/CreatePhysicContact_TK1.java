
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.*;

import java.util.stream.Collectors;

@DisplayName("ТК_А1 - Создание физического лица")
public class CreatePhysicContact_TK1 extends TestBase {

    @Test
    @DisplayName("ТК_А1 - Создание физического лица")
    public void createPhysicContact() throws InterruptedException {
        //arrange
        String lastName = "Авто";
        String firstName = "Тест";
        String patronymic = "Тестович";
        String categoryOfContactForDrop = "employee";
        String phone = "9999999999";
        String email = "xxx@xxx.xx";
        //Открыть тестовый стенд
        MainPage mainPage = new MainPage(this.driver, this.wait);
        mainPage.openPage()
                .authorize()
                .navigationPanel()
                .toContacts();

        // Раздел создать физ лицо
        CreateContactPage createContactPage = new CreateContactPage(this.driver, this.wait);
        // В меню модуля выбрать пункт «Создать новое физическое лицо».
        createContactPage.clickMakeContact();
        // В форме «Основная информация» заполнить поле «Фамилия» - Авто.
        createContactPage.setLastNameInput(lastName);
        // В разделе «Основная информация» заполнить поле «Имя» - Тест.
        createContactPage.setFirstNameInput(firstName);
        // В разделе «Основная информация» заполнить поле «Отчество» - Тестович.
        createContactPage.setSecondNameInput(patronymic);
        // В разделе «Основная информация» заполнить поле «Категория контакта» - нажать на поле и выбрать из выпадающего списка значение «Сотрудник предприятия»
        createContactPage.setCategoryOfContactDrop(categoryOfContactForDrop);
        // В разделе «Основная информация» заполнить поле «Предприятие» - нажать на кнопку со стрелкой справа от поля.
        createContactPage.clickButtonCompany();
        // сохраняем текущее окно
        var initialWindow = driver.getWindowHandle();
        // сохраняем все окна
        var allWindows = driver.getWindowHandles();
        // определяем открытое окно
        var otherWindows = allWindows.stream().filter(x -> !x.equals(initialWindow)).collect(Collectors.toList());
        //переключение на 2 окно
        driver.switchTo().window(otherWindows.stream().findFirst().get());
        //окно с поиском предприятий
        EnterpriseSearchWindowPage enterpriseSearchWindowPage = new EnterpriseSearchWindowPage(this.driver, this.wait);
        // Заполнить поле «ИНН» - 6321277661
        enterpriseSearchWindowPage.setInnAdvanced();
        // Нажать на кнопку «Найти».
        enterpriseSearchWindowPage.clickSearchButton();
        // Выбрать найденное предприятие.
        enterpriseSearchWindowPage.clickFindedCompany();
        // Переходим в раздел "Создать физ лицо".
        driver.switchTo().window(initialWindow);
        // В форме «Основная информация» заполнить поле «Должность» - менеджер.
        createContactPage.setPosition();
        // В разделе «Контактные данные» заполнить по маске ХХХХХХХХХХ поле «Телефон»
        createContactPage.setPhone(phone);
        // В разделе «Контактные данные» заполнить поле «E-mail» по маске  XXX@XXX.XX.
        createContactPage.setEmail(email);
        // Под Формой создания физического лица нажать на кнопку «Сохранить и выйти».
        createContactPage.clickSaveExit();

        //assert
        Assertions.assertAll(
                //Проверить, что поля, заполнены в соответствии с указанными на форме:
                // Фамилия
                () -> Assertions.assertTrue(createContactPage.getLastName().contains("АВТО"), "Фамилия с больших букв или не верное"),
                // Имя
                () -> Assertions.assertTrue(createContactPage.getFirstName().contains("ТЕСТ"), "Имя большими буквами или не верное"),
                // Отчество
                () -> Assertions.assertTrue(createContactPage.getSecondName().contains("ТЕСТОВИЧ"), "Отчество большими буквами или не верное"),
                // Категория контакта
                () -> Assertions.assertTrue(createContactPage.getCategoryOfContact().contains("Сотрудник предприятия"), "Категория маленькими или не верное"),
                // Предприятие
                () -> Assertions.assertTrue(createContactPage.getCompanyInput().contains("ООО \"СЕНТЯБРИНКА\""), "Предприятие большими или не верное"),
                // Телефон
                () -> Assertions.assertTrue(createContactPage.getPhoneNumberInput().contains(phone), "Телефон не верный"),
                // Email
                () -> Assertions.assertTrue(createContactPage.getEmail().contains(email), "Email не верный")
        );
//
        //После теста- удаление контакта
        //страница физ лица
        DellContactPage dellContactPage = new DellContactPage(this.driver);
        //наведение курсора на дроп Действия -> клик по Удалить
        dellContactPage.dellContact();
        //переключение на алерт и нажать удалить
        driver.switchTo().alert().accept();
    }


}
