
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.Selenide.AuthorizationSelenidePage;
import pages.Selenide.CreateAppealPageSelenidePage;
import pages.Selenide.MainPageSelenidePage;

import static com.codeborne.selenide.Selenide.*;

@DisplayName("ТК_А7 - Взятие в работу обращения и вынесение решения")
public class TakingAccountAppealMakingDecision_TK7 extends TestBaseSelenide {
    /**
     * Тест на Selenide
     */
    protected String url = "http://fckproject.itfbgroup.ru/auto/";

    @Test
    @DisplayName("ТК_А7 - Взятие в работу обращения и вынесение решения")
    public void accountAppealMakingDecision() {
        //arrange
        String fio = "Ласточкин Борис Васильевич";
        String value = "fck";
        String consultValue = "consult";
        String workPlatform = "consult_tech";
        String consultRegistrationValue = "consult_tech_registration";
        String text = "Текст описания";
        String emailValue = "email";
        // Открыть тестовый стенд http://fckproject.itfbgroup.ru/fcktest_001/.
        MainPageSelenidePage mainPageSelenidePage = new MainPageSelenidePage();
        open(url);
        // Авторизация
        new AuthorizationSelenidePage();

        //act
        // Выбрать закладку модулей «Все».
        mainPageSelenidePage.navigationPanelAllSelenidePage.allMenuS();
        // Из выпадающего списка выбрать пункт «Обращения».
        mainPageSelenidePage.navigationPanelAllSelenidePage.clickToAppealsMenuS();
        // Переход в меню «Создать обращение».
        CreateAppealPageSelenidePage createAppealPageSelenidePage = new CreateAppealPageSelenidePage();
        // В меню модуля выбрать пункт «Создать обращение».
        createAppealPageSelenidePage.clickCreateAppealS();
        // В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.
        createAppealPageSelenidePage.clickToArrowContactS();
        // переключаемся на другое окно
        switchTo().window(1);
        // В поле «ФИО» ввести Фамилию физического лица из предусловия.
        createAppealPageSelenidePage.setSearchFioS(fio);
        // Нажать на кнопку «Найти»
        createAppealPageSelenidePage.clickButtonSearchS();
        // Выбрать физическое лицо из формы просмотра списка физических лиц.
        createAppealPageSelenidePage.clickFindedFioS();
        // переключение на 1 окно
        switchTo().window(0);
        // В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
        createAppealPageSelenidePage.clickCategoryS();
        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
        createAppealPageSelenidePage.setSelectEmployeeFckS(value);
        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
        createAppealPageSelenidePage.clickSubtypeS();
        // Выбрать из выпадающего списка значение «Консультация».
        createAppealPageSelenidePage.setConsultValueS(consultValue);
        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
        createAppealPageSelenidePage.clickSubjectSelectorS();
        // Выбрать из выпадающего списка значение «Работа платформы».
        createAppealPageSelenidePage.setSelectSubjectEventS(workPlatform);
        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
        createAppealPageSelenidePage.clickSubSubjectS();
        // Выбрать из выпадающего списка значение «Регистрация на портале».
        createAppealPageSelenidePage.setSelectSubSubjectS(consultRegistrationValue);
        // Переключение на фрейм 3
        switchTo().frame(2);
        // В поле «Описание» вручную ввести текст – «Текст описания
        createAppealPageSelenidePage.setTinymceValueS(text);
        // Переключение на фрейм 1
        switchTo().defaultContent();
        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
        createAppealPageSelenidePage.clickConnectS();
        // Из выпадающего списка выбрать «Email».
        createAppealPageSelenidePage.setConnectValueS(emailValue);
        // В поле «Email для связи» указать Email из предусловия.
        createAppealPageSelenidePage.setEmailForConnectS();
        // Нажать на кнопку «Сохранить и выйти».
        createAppealPageSelenidePage.clickSaveAndExitS();
        // В форме просмотра нажать на кнопку «Взять в работу».
        createAppealPageSelenidePage.workPlatformSelenidePage.clickButtonTakeWork();
        // В разделе «Решение» в поле «Решение» ввести решение по обращению - «Текст решения».
        createAppealPageSelenidePage.workPlatformSelenidePage.setTextIntoTextarea(text);
        // Нажать на кнопку «Сохранить и выйти».
        createAppealPageSelenidePage.workPlatformSelenidePage.clickExitAndSave();

        // Asserts
        // Поле «Состояние» заполнено – «Закрыто».
        Assertions.assertTrue(createAppealPageSelenidePage.workPlatformSelenidePage.state.getAttribute("value").equals("Closed"),
                "Поле «Состояние» не заполнено – «Закрыто». ");
        // Поле «Статус» заполнено – «Решено».
        Assertions.assertTrue(createAppealPageSelenidePage.workPlatformSelenidePage.status.getAttribute("value").equals("Closed_Closed"),
                "Поле «Статус» не заполнено – «Решено»");
        // Поле «Ответственный» заполнено – указан текущий пользователь (Автотест)
        Assertions.assertTrue(createAppealPageSelenidePage.workPlatformSelenidePage.assignedUser.getText().equals("Тменов Дмитрий"),
                "Поле «Ответственный» не заполнено – указан текущий пользователь (Автотест)");
    }
}
