package db_fck;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import java.sql.Date;
import java.time.DayOfWeek;
import java.time.LocalDate;

@Slf4j
public class TestDB_TK9 {
    // Проверить заполнение поля «Крайний срок обработки».
    public void assertProcessingDeadlineField(Integer intervalProcessing, Date processingDeadline) {
        /**
         * Проверка текущего и сле. дня
         */
        LocalDate currentDate = LocalDate.now();
        DayOfWeek currentDayOfWeek = currentDate.getDayOfWeek();

        // Текущий день плюс интервал
        Date currentDatePlusInterval = null;

        if (currentDayOfWeek == DayOfWeek.SATURDAY || currentDayOfWeek == DayOfWeek.SUNDAY
                || currentDayOfWeek.plus(1) == DayOfWeek.SUNDAY) {
            log.info("Today (" + currentDate + ") это выходной");
            // проверка является ли текущий день выходным (суббота или воскресенье);
            //       - если да, то происходит проверка является ли следующий день выходным;
            //              - если да, то Крайний срок обращения высчитывается по формуле
            //              = Дата создания обращения + значение из поля "Интервал обработки, дни" + 2 дня;
            currentDatePlusInterval = Date.valueOf(currentDate.plusDays(intervalProcessing));
            log.info("Прибавляем к текущей дате знчение интервала = " + currentDatePlusInterval);
            Assertions.assertEquals(processingDeadline, currentDatePlusInterval);
        } else {
            // если нет, то Крайний срок обращения высчитывается по формуле=
            // Дата создания обращения + значение из поля "Интервал обработки, дни" + 1 день)
            log.info("Today (" + currentDate + ") не является выходным днем -> " +
                    "Значит + 1 день = " + currentDate.plusDays(1));
        }

        log.info("Интервал обработки, дни = " + intervalProcessing + " Крайний срок обработки = " + processingDeadline);
    }

    // Проверить заполнение поля «Вес обращения».
    public void assertWeightField(Integer intervalProcessing, Integer weightAppeal, Date processingDeadline) {
        Integer expectProcessingDeadline = intervalProcessing + weightAppeal;

//        Assertions.assertEquals(expectProcessingDeadline,);
    }
}
