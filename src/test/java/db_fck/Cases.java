package db_fck;

import java.sql.Date;


public class Cases {

    private Date processingDeadline;
    private Date odsTimeManagement;

    public Cases() {}

    public Date getProcessing_deadline() {
        return processingDeadline;
    }

    public Date getOdsTimeManagement() {
        return odsTimeManagement;
    }
}
