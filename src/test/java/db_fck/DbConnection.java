package db_fck;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static readProperties.ConfigProviders.*;

public class DbConnection {
    // local DB
    private final String url = "jdbc:mysql://localhost:3306/test_base";
    private final String user = "root";
    private final String pass = "46827598mysqL";

    // remote DB
    private Connection connection;


    public void remoteConnectionConfig() {
        {
            try {
                connection = DriverManager.getConnection(HOST_REMOTE
                        , USER_LOGIN, PASSPORT_DB);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void localConnectionConfig() {
        {
            try {
                connection = DriverManager.getConnection(HOST_LOCAL
                        , USER_DB_LOGIN, PASSPORT_DB_LOCAL);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() {
        return connection;
    }

    // JBDCTemplate_forREMOTE
    public JdbcTemplate jdbcTemplate() {
        remoteConnectionConfig();
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(HOST_REMOTE);
        dataSource.setUsername(USER_LOGIN);
        dataSource.setPassword(PASSPORT_DB);

        return new JdbcTemplate(dataSource);
    }

    // JBDCTemplate_forLOCAL
    public JdbcTemplate jdbcTemplateForLocal() {
        localConnectionConfig();

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(HOST_LOCAL);
        dataSource.setUsername(USER_DB_LOGIN);
        dataSource.setPassword(PASSPORT_DB_LOCAL);

        return new JdbcTemplate(dataSource);
    }

}
