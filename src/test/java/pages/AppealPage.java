package pages;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.SelectOption;
import io.qameta.allure.Step;

public class AppealPage {
    private final Page page;

    // locators
    protected String appealSelector = "(//*[contains(text(),\"Обращения\")])[3]";
    protected String createSelector = "(//*[contains(@class,'actionmenulink')])[1]";
    protected String arrowContactFace = "(//*[contains(@class,'suitepicon-action-select')])[2]";
    protected String category = "#category";
    protected String podtip = "#subtype";
    protected String tema = "#subject";
    protected String subTema = "#subsubject";
    protected String fillText = "#tinymce";
    protected String iframe = "(//*/iframe)[3]";
    protected String desireMethodOfCommunication = "#connect";
    protected String saveAndExit = "(//*[contains(@class,'button primary')])[2]";



    // constructor
    public AppealPage(Page page) {
        this.page = page;
    }

    @Step("Из выпадающего списка выбрать пункт «Обращения».")
    public void clickToAppeal() {
        page.locator(appealSelector).click();
    }

    @Step("В меню модуля выбрать пункт «Создать обращение».")
    public void createToAppeal() {
        page.locator(createSelector).click();
    }


    @Step("В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.")
    public void clickContactFaceArrow() {
        page.locator(arrowContactFace).click();
    }

    @Step("В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»")
    public void clickCategory() {
        page.locator(category).click();
    }

    @Step("Выбрать из выпадающего списка значение «Сотрудник ФЦК»")
    public void setCategoryFckEmployer() {
        page.locator(category).selectOption("Сотрудник ФЦК");
    }

    @Step("В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».")
    public void clickPodtip() {
        page.locator(podtip).click();
    }

    @Step("Выбрать из выпадающего списка значение «Консультация».")
    public void setConsultation() {
        page.locator(podtip).selectOption("Консультация");
    }

    @Step("В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».")
    public void clickTema() {
        page.locator(tema).click();
    }

    @Step("Выбрать из выпадающего списка значение «Работа платформы».")
    public void setWorkPlatform() {
        page.locator(tema).selectOption("Работа платформы");
    }

    @Step("В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».")
    public void clickSubTem() {
        page.locator(subTema).click();
    }

    @Step("Выбрать из выпадающего списка значение «Регистрация на портале».")
    public void setRegistrationOnPortal() {
        page.locator(subTema).selectOption("Регистрация на портале");
    }


    @Step("В поле «Описание» вручную ввести текст – «Текст описания»")
    public void fillText() {
        page.frameLocator(iframe).locator(fillText).fill("Текст описания");
    }

    @Step("В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».")
    public void desireMethodOfCommunication() {
        page.locator(desireMethodOfCommunication).click();
    }

    @Step("В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».")
    public void setDesireMethodOfCommunication() {
        page.locator(desireMethodOfCommunication).click();
    }

    @Step("Из выпадающего списка выбрать «Email».")
    public void setEmail() {
        page.locator(desireMethodOfCommunication).selectOption("Email");
    }

    @Step("В поле «Email для связи» указать Email из предусловия.")
    public void selectEmail() {
        page.selectOption("#contact_emails",new SelectOption().setIndex(1));
    }

    @Step("Нажать на кнопку «Сохранить и выйти».")
    public void clickSaveAndExit() {
        page.locator(saveAndExit).click();
    }





}
