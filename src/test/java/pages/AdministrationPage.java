package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdministrationPage extends MainPage {

    @FindBy(css = "#configphp_settings")
    private WebElement configurationSetting;

    @FindBy(xpath = "//*[@target='_blank']")
    private WebElement watchLogs;

    @FindBy(name = "mark")
    private WebElement mark;

    @FindBy(tagName = "html")
    private WebElement page;

    public AdministrationPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Step("В разделе меню «Система» выбрать «Настройка конфигурации».")
    public void clickConfigSetting() {
        configurationSetting.click();
    }

    public void scrollDown() {
        for (int i = 0; i < 300; i++) {
            page.sendKeys(Keys.ARROW_DOWN);
        }
    }

    @Step("Внизу формы просмотра кликнуть на «Просмотр журнала».")
    public void clickWatchLogs() {
        new Actions(driver).moveToElement(watchLogs).click().perform();
    }

    @Step("Нажать на кнопку «Установить контрольную точку».")
    public void clickSetControlPoint() {
        mark.click();
    }
}
