package pages;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class MainPagePW {
    private final Page page;

    // locators
    protected String allButton = "#grouptab_1";

    // constructor
    public MainPagePW(Page page) {
        this.page = page;
    }

    public void setUrl(String url) {
        page.navigate(url);
    }

    @Step("Выбрать закладку модулей «Все»")
    public void hoverToAll() {
        page.locator(allButton).hover();
    }
}
