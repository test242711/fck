package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EnterpriseSearchWindowPage extends MainPage {
    protected final String inn = "6321277661";

    @FindBy(css = "#inn_advanced")
    private WebElement innAdvanced;

    @FindBy(css = "#search_form_submit")
    private WebElement searchCompanyFormSubmit;

    @FindBy(css = ".oddListRowS1 a")
    private WebElement listWithCompany;

    public EnterpriseSearchWindowPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Step("Заполнить поле «ИНН» - 6321277661")
    public void setInnAdvanced() {
        innAdvanced.sendKeys(inn);
    }

    @Step("Нажать на кнопку «Найти».")
    public void clickSearchButton() {
        searchCompanyFormSubmit.click();
    }

    @Step("Выбрать найденное предприятие.")
    public void clickFindedCompany() {
        listWithCompany.click();
    }
}
