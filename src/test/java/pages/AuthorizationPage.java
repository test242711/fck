package pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.qameta.allure.Step;
import readProperties.ConfigProviders;


public class AuthorizationPage extends MainPage {
    private String name = ConfigProviders.LOGIN;
    private String pass = ConfigProviders.PASSPORT;

    //locator
    @FindBy(id = "user_name")
    private WebElement login;

    @FindBy(id = "username_password")
    private WebElement password;

    @FindBy(id = "bigbutton")
    private WebElement button;


    public AuthorizationPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver,this);
    }


    @Step("Авторизация")
    public NavigationPanelAllPage authorize() {
        this.login.sendKeys(name);
        this.password.sendKeys(pass, Keys.ENTER);
        return new NavigationPanelAllPage(driver, wait);
    }


}
