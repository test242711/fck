package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import readProperties.ConfigProviders;

public class MainPage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected String url = ConfigProviders.URL;
    protected String subUrl = "";

    public MainPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }


    //metods
    public AuthorizationPage openPage() {
        this.driver.navigate().to(this.getPageUrl());
        this.driver.get(getPageUrl());
        return new AuthorizationPage(driver, wait);
    }

    protected String getPageUrl() {
        return this.url + this.subUrl;
    }



}



