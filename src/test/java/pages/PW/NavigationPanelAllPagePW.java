package pages.PW;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class NavigationPanelAllPagePW {
    protected Page page;

    //locator
    protected String physicalPerson = "(//*[contains(@id,'moduleTab_9_Физические лица')])[2]";

    // constructor
    public NavigationPanelAllPagePW(Page page) {
        this.page = page;
    }

    @Step("В выпадающем списке выбрать пункт «Физические лица» («Contacts»).")
    public void clickToPhysicalPerson() {
        page.locator(physicalPerson).click();
    }
}
