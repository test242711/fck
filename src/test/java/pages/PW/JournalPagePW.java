package pages.PW;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class JournalPagePW {
    private final Page page;

    // locators
    protected String buttonSetControlPoint = "//*[@name='mark']";

    // constructor
    public JournalPagePW(Page page) {
        this.page = page;
    }

    @Step("Нажать на кнопку «Установить контрольную точку».")
    public void clickToConfigureSetting() {
        page.locator(buttonSetControlPoint).click();
    }


}
