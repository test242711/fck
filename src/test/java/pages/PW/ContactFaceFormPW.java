package pages.PW;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class ContactFaceFormPW extends PhysicalPersonPW {
    private final Page page;

    // locators
    protected String fullName = "#full_name_advanced";
    protected String buttonSaveAndExit = "#search_form_submit";
    protected String phisFace = "(//*[contains(@class,'oddListRowS1')] / a)[1]";

    public ContactFaceFormPW(Page page) {
        super(page);
        this.page = page;
    }

    @Step("В поле «ФИО» ввести %<Фамилия>%, где <Фамилия>=Фамилия, созданного физического лица.")
    public void setFIO() {
        page.locator(fullName).fill(lastNameRndm);
    }

    @Step("Нажать на кнопку «Найти»")
    public void saveAndExit() {
        page.locator(buttonSaveAndExit).click();
    }

    @Step("Выбрать физическое лицо, созданное ранее, из формы просмотра списка физических лиц.\n")
    public void getPhisFace() {
        page.locator(phisFace).click();
    }
}
