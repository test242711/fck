package pages.PW;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class PhysicalPersonPW {
    protected Page page;
    protected String lastNameRndm = "Фамилияz";
    protected String nameRndm = "ИмяИмяz";
    protected String secondName = "Отчествоz";

    // locator
    protected String createPhysic = "(//*[contains(@class,'actionmenulink')])[2]";
    protected String lastName = "#last_name";
    protected String name = "#first_name";
    protected String second = "#second_name";
    protected String setValue = "Сотрудник предприятия";
    protected String dropCategory = "#contact_category";
    protected String company = "(//*[contains(@class,'suitepicon suitepicon-action-select')])[1]";
    protected String inn = "#inn_advanced";
    protected String searchFormSubmit = "#search_form_submit";
    protected String firstCompany = "(//*[contains(@class,'oddListRowS1')] // a)";
    protected String postManager = "#position";
    protected String contactDate = ".form-control.phone_number";
    protected String email = "#Contacts0emailAddress0";
    protected String saveAndExit = "(//*[contains(@id,'SAVE')])[1]";

    // constructor
    public PhysicalPersonPW(Page page) {
        this.page = page;
    }

    @Step("В меню модуля выбрать пункт «Создать новое физическое лицо».")
    public void clickCreatePhysic() {
        page.locator(createPhysic).click();
    }

    @Step("В разделе «Основная информация» заполнить поле «Фамилия» - рандомный набор букв на кириллице от 3 до 12 символов.")
    public void setRandomLastName() {
        page.locator(lastName).fill(lastNameRndm);
    }

    @Step("В разделе «Основная информация» заполнить поле «Имя» - рандомный набор букв на кириллице от 3 до 12 символов.")
    public void setName() {
        page.locator(name).fill(nameRndm);
    }

    @Step("В разделе «Основная информация» заполнить поле «Отчество» - рандомный набор букв на кириллице от 3 до 12 символов.")
    public void setSecondName() {
        page.locator(second).fill(secondName);
    }

    @Step("В разделе «Основная информация» заполнить поле «Категория контакта» - нажать на поле и выбрать из выпадающего списка значение «Сотрудник предприятия»")
    public void setCategoryOnDrop() {
        page.locator(dropCategory).selectOption(setValue);
    }

    @Step("В разделе «Основная информация» заполнить поле «Предприятие» - нажать на кнопку со стрелкой справа от поля.")
    public void clickCompanyArrow() {
        page.locator(company).click();
    }

    @Step("Заполнить поле «ИНН» - 6321277661.")
    public void setInn() {
        page.locator(inn).fill("6321277661");
    }

    @Step("Нажать на кнопку «Найти».")
    public void clickButton() {
        page.locator(searchFormSubmit).click();
    }

    @Step("Выбрать найденное предприятие.")
    public void clickCompany() {
        page.locator(firstCompany).click();
    }

    @Step("В разделе «Основная информация» заполнить поле «Должность» - менеджер.")
    public void fillPostManager() {
        page.locator(postManager).fill("менеджер");
    }

    @Step("В разделе «Контактные данные» заполнить по маске ХХХХХХХХХХ поле «Телефон»")
    public void fillPhone() {
        page.locator(contactDate).fill("9999999999");
    }

    @Step("В подразделе «Контактные данные» заполнить поле «E-mail» по маске  XXX@XXX.XX.")
    public void fillEmail() {
        page.locator(email).fill("rrr@rr.com");
    }

    @Step("Под Формой создания физического лица нажать на кнопку «Сохранить и выйти».")
    public void saveAndExit() {
        page.locator(saveAndExit).click();
    }


}
