package pages.PW;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class SystemMenuPagePW {
    private final Page page;

    // locators
    protected String buttonSystem = "#configphp_settings";
    protected String buttonJournal = "(//*[@target='_blank'])[1]";

    // constructor
    public SystemMenuPagePW(Page page) {
        this.page = page;
    }

    @Step("В разделе меню «Система» выбрать «Настройка конфигурации».")
    public void clickToConfigureSetting() {
        page.locator(buttonSystem).click();
    }

    @Step("Внизу формы просмотра кликнуть на «Просмотр журнала».")
    public void clickToSeeJournal() {
        page.locator(buttonJournal).click();
    }




}
