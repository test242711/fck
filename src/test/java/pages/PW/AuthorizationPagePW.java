package pages.PW;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class AuthorizationPagePW {
    private final Page page;

    private String name = "d.tmenov";
    private String pass = "123Pass";

    // locators
    protected String login = "#user_name";
    protected String passport = "#username_password";
    protected String button = "id=bigbutton";

    public AuthorizationPagePW(Page page) {
        this.page = page;
        page.fill(login, name);
        page.fill(passport, pass);
        page.locator(button).click();
    }

}
