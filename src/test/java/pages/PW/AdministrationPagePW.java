package pages.PW;

import com.microsoft.playwright.Page;

public class AdministrationPagePW {
    private final Page page;

    // locators
    protected String button1 = "(//*[contains(@id,'with-label')])[1]";
    protected String button2 = "(//*[contains(@id,'admin_link')])[3]";

    public AdministrationPagePW(Page page) {
        this.page = page;
        page.locator(button1).click();
        page.locator(button2).click();
    }
}
