package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CreateContactPage extends MainPage {
    protected final String position = "менеджер";

    @FindBy(css = ".actionmenulinks:nth-of-type(1)")
    private WebElement addContact;

    @FindBy(css = "#last_name")
    private WebElement lastNameInput;

    @FindBy(css = "#first_name")
    private WebElement firstNameInput;

    @FindBy(css = "#second_name")
    private WebElement secondNameInput;

    @FindBy(css = "#contact_category")
    private WebElement contactCategoryDrop;

    @FindBy(xpath = "//*[contains(@type,'enum')]")
    private WebElement categoryOfContact;

    @FindBy(css = "#btn_account_name")
    private WebElement btnAccountName;

    @FindBy(css = "#position")
    private WebElement positionInput;

    @FindBy(css = ".phone_number")
    private WebElement phoneNumberInput;

    @FindBy(css = ".multiphone-detail")
    private WebElement phoneGetNumberInput;

    @FindBy(css = "#Contacts0emailAddress0")
    private WebElement emailAddress;

    @FindBy(xpath = "(//*[contains(@id,'SAVE')])[2]")
    private WebElement saveAndExitButton;

    @FindBy(css = ".oddListRowS1")
    private WebElement savedContact;

    @FindBy(css = "#account_id")
    private WebElement companyInput;

    @FindBy(css = ".email-link")
    private WebElement emailGetInput;

    public CreateContactPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Step("Под Формой создания физического лица нажать на кнопку «Сохранить и выйти».")
    public void clickSaveExit() {
        new Actions(driver)
                .moveToElement(saveAndExitButton)
                .click()
                .perform();
    }

    @Step("В меню модуля выбрать пункт «Создать новое физическое лицо».")
    public void clickMakeContact() {
        new Actions(driver)
                .moveToElement(addContact)
                .click()
                .perform();
    }

    @Step("В форме «Основная информация» заполнить поле «Фамилия» - {lastName}.")
    public void setLastNameInput(String lastName) {
        lastNameInput.sendKeys(lastName + getDateAndTime());
    }

    @Step("В разделе «Основная информация» заполнить поле «Имя» - {firstName}.")
    public void setFirstNameInput(String firstName) {
        firstNameInput.sendKeys(firstName + getDateAndTime());
    }

    @Step("В разделе «Основная информация» заполнить поле «Отчество» - {patronymic}.")
    public void setSecondNameInput(String patronymic) {
        secondNameInput.sendKeys(patronymic + getDateAndTime());
    }

    @Step("В разделе «Основная информация» заполнить поле «Категория контакта» - " +
            "нажать на поле и выбрать из выпадающего списка значение - {categoryOfContactForDrop}")
    public void setCategoryOfContactDrop(String categoryOfContactForDrop) {
        new Select(contactCategoryDrop)
                .selectByValue(categoryOfContactForDrop);
    }

    @Step("В разделе «Основная информация» заполнить поле «Предприятие» - нажать на кнопку со стрелкой справа от поля.")
    public void clickButtonCompany() {
        btnAccountName.click();
    }

    @Step("В форме «Основная информация» заполнить поле «Должность» - менеджер.")
    public void setPosition() {
        positionInput.sendKeys(position);
    }

    @Step("В разделе «Контактные данные» заполнить по маске ХХХХХХХХХХ поле {phone}")
    public void setPhone(String phone) {
        phoneNumberInput.sendKeys(phone);
    }

    @Step("В разделе «Контактные данные» заполнить поле «E-mail» = {email} по маске  XXX@XXX.XX.")
    public void setEmail(String email) {
        emailAddress.sendKeys(getDateAndTime() + email);
    }

    public String getFirstName() {
        return firstNameInput.getText();
    }

    public String getLastName() {
        return lastNameInput.getText();
    }

    public String getSecondName() {
        return secondNameInput.getText();
    }

    public String getCategoryOfContact() {
        return categoryOfContact.getText();
    }

    public String getCompanyInput() {
        return companyInput.getText();
    }

    public String getPhoneNumberInput() {
        return phoneGetNumberInput.getText();
    }

    public String getEmail() {
        return emailGetInput.getText();
    }

    public String getDateAndTime() {
        LocalDateTime now = LocalDateTime.now();
        String nextDay = now.format(DateTimeFormatter.ofPattern(" ddMMyyyy_HHmmssSSS"));
        return nextDay;
    }


}
