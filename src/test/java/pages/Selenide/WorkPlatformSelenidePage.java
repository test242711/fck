package pages.Selenide;

import com.codeborne.selenide.Selectors;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class WorkPlatformSelenidePage {
    public SelenideElement takeToWork = $(Selectors.byXpath("(//*[contains(@class,'button')])[20]"));
    public SelenideElement textAreaDecision = $("#resolution");
    public SelenideElement exit = $(Selectors.byXpath("(//*[contains(@class,'primary')])[2]"));
    public SelenideElement state = $("#state");
    public SelenideElement status = $("#status");
    public SelenideElement assignedUser = $("#assigned_user_id");


    @Step("В форме просмотра нажать на кнопку «Взять в работу».")
    public void clickButtonTakeWork() {
        takeToWork.click();
    }

    @Step("В разделе «Решение» в поле «Решение» ввести решение по обращению - «Текст решения».")
    public void setTextIntoTextarea(String text) {
        textAreaDecision.setValue(text);
    }

    @Step("Нажать на кнопку «Сохранить и выйти».")
    public void clickExitAndSave() {
        exit.click();
    }

}
