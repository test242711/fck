package pages.Selenide;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class AuthorizationSelenidePage {
    private final String name = "d.tmenov";
    private final String pass = "123Pass";

    public SelenideElement log = $("#user_name");
    public SelenideElement pas = $("#username_password");
    public SelenideElement but = $("#bigbutton");

    public AuthorizationSelenidePage() {
        log.sendKeys(name);
        pas.sendKeys(pass);
        but.click();
    }
}
