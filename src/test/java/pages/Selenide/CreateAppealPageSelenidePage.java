package pages.Selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class CreateAppealPageSelenidePage extends MainPageSelenidePage {

    public SelenideElement createAppealS = $(byXpath("(//*[contains(@class,'actionmenulink')])[1]"));
    public SelenideElement subtypeSelectorS = $("#subtype");
    public SelenideElement subjectSelectorS = $("#subject");
    public SelenideElement tinymceS = $("#tinymce p");
    public SelenideElement connectS = $("#connect");
    public SelenideElement emailForConnectS = $("#contact_emails");
    public SelenideElement saveAndExitS = $(byXpath("(//*[contains(@class,\"button primary\")])[2]"));
    public SelenideElement arrowContactFaceS = $("#btn_contact_created_by_name");
    public SelenideElement inputFioSearchS = $("#full_name_advanced");
    public SelenideElement buttonSearchS = $("#search_form_submit");
    public SelenideElement firstFindFioS = $(byXpath("(//*[contains(@class, \"oddListRowS1\")] // a)[1]"));
    public SelenideElement categoryS = $("#category");
    public SelenideElement subtypeS = $("#subtype");
    public SelenideElement subjectS = $("#subject");
    public SelenideElement subsubjectS = $("#subsubject");

    public CreateAppealPageSelenidePage() {

    }

    @Step("В меню модуля выбрать пункт «Создать обращение».")
    public void clickCreateAppealS() {
        createAppealS.shouldBe(Condition.visible).click();
    }

    @Step("Выбрать из выпадающего списка значение «Консультация» = {value}.")
    public void setConsultValueS(String value) {
        subtypeSelectorS.selectOptionByValue(value);
    }

    @Step("В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».")
    public void clickSubjectSelectorS() {
        subjectSelectorS.scrollTo().hover();
    }

    @Step("В поле «Описание» вручную ввести текст – «Текст описания = {text}")
    public void setTinymceValueS(String text) {
        tinymceS.sendKeys(text);
    }

    @Step("В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».")
    public void clickConnectS() {
        connectS.click();
    }

    @Step("Из выпадающего списка выбрать «Email» = {email}.")
    public void setConnectValueS(String email) {
        connectS.selectOptionByValue(email);
    }

    @Step("В поле «Email для связи» выбрать из выпадающего списка сохраненный Email.")
    public void setEmailForConnectS() {
        emailForConnectS.selectOption(1);
    }

    @Step("Нажать на кнопку «Сохранить и выйти».")
    public void clickSaveAndExitS() {
        saveAndExitS.scrollTo().click();
    }

    @Step("В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.")
    public void clickToArrowContactS() {
        arrowContactFaceS.click();
    }

    @Step("В поле «ФИО» ввести фамилию физического лица из предусловия = {fio}.")
    public void setSearchFioS(String fio) {
        inputFioSearchS.sendKeys(fio);
    }

    @Step("Нажать на кнопку «Найти»")
    public void clickButtonSearchS() {
        buttonSearchS.click();
    }

    @Step("Выбрать физическое лицо из формы просмотра списка физических лиц.")
    public void clickFindedFioS() {
        firstFindFioS.click();
    }

    @Step("В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»")
    public void clickCategoryS() {
        categoryS.click();
    }

    @Step("Выбрать из выпадающего списка значение «Сотрудник ФЦК» = {value}")
    public void setSelectEmployeeFckS(String value) {
        categoryS.selectOptionByValue(value);
    }

    @Step("В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».")
    public void clickSubtypeS() {
        subtypeS.click();
    }

    @Step("Выбрать из выпадающего списка значение «Работа ФЦК» = value.")
    public void setSelectSubjectEventS(String value) {
        subjectS.selectOptionByValue(value);
    }

    @Step("В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».")
    public void clickSubSubjectS() {
        subsubjectS.hover();
    }

    @Step("Выбрать из выпадающего списка выбрать значение «Другое» = {value}.")
    public void setSelectSubSubjectS(String value) {
        subsubjectS.selectOptionByValue(value);
    }


}
