package pages.Selenide;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class NavigationPanelAllSelenidePage {
    protected WebDriver driver;

    public SelenideElement appealsMenuS = $(byXpath("(//*[contains(text(),\"Обращения\")])[3]"));

    public SelenideElement allMenuS = $("#grouptab_1");

    public NavigationPanelAllSelenidePage() {}

    @Step("На основной закладке домашней страницы выбрать закладку модулей «Все».")
    public void allMenuS() {
        allMenuS.hover();
    }

    @Step("Из выпадающего списка выбрать пункт «Обращения».")
    public void clickToAppealsMenuS() {
        appealsMenuS.scrollTo().click();
    }
}
