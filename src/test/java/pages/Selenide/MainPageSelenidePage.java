package pages.Selenide;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPageSelenidePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    public NavigationPanelAllSelenidePage navigationPanelAllSelenidePage;
    public WorkPlatformSelenidePage workPlatformSelenidePage;

    protected String url = "http://fckproject.itfbgroup.ru/auto/";

    protected String subUrl = "";


    // for Selenide
    public MainPageSelenidePage() {
        navigationPanelAllSelenidePage = new NavigationPanelAllSelenidePage();
        workPlatformSelenidePage = new WorkPlatformSelenidePage();
    }

    //metods
    public void open() {
        this.driver.navigate().to(this.getPageUrl());
        this.driver.get(getPageUrl());
    }

    protected String getPageUrl() {
        return this.url + this.subUrl;
    }
}
