import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.AuthorizationPage;
import pages.CreateAppealPage;
import pages.MainPage;

import java.util.stream.Collectors;

@DisplayName("ТК_А4- Создание обращения от ФЛ с ЛК")
public class CreatingAnAppealFromFLWithLC_TK4 extends TestBase {

    @Test
    @DisplayName("ТК_А4- Создание обращения от ФЛ с ЛК")
    public void createAppealFromFLtoLC() {
        //arrange
        String fio = "Ласточкин Борис Васильевич";
        String email = "testlkvp@mail.ru";
        String value = "fck";
        String invitationValue = "invitation";
        String invitation_eventsValue = "invitation_events";
        String invitation_events_otherValue = "invitation_events_other";
        String text = "Текст описания";
        String emailValue = "email";

        //Открыть тестовый стенд
        MainPage mainPage = new MainPage(this.driver, this.wait);
        mainPage.openPage()
                .authorize()
                .navigationPanel()
                .toContacts();
//        // авторизация
//        AuthorizationPage authorizationPage = new AuthorizationPage(this.driver, this.wait);
//        authorizationPage.authorize();
//
//        //act
//        // На основной закладке домашней страницы выбрать закладку модулей «Все».
//        mainPage.navigationPanelAllPage.allMenu();
//        // Из выпадающего списка выбрать пункт «Обращения».
//        mainPage.navigationPanelAllPage.clickToAppealsMenu();
        // Переход в меню «Создать обращение».
        CreateAppealPage createAppealPage = new CreateAppealPage(this.driver, this.wait);
        // В меню модуля выбрать пункт «Создать обращение».
        createAppealPage.clickCreateAppeal();
        // В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.
        createAppealPage.clickToArrowContact();
        // сохраняем текущее окно
        var initialWindow = driver.getWindowHandle();
        // сохраняем все окна
        var allWindows = driver.getWindowHandles();
        // определяем открытое окно
        var otherWindows = allWindows.stream().filter(x -> !x.equals(initialWindow)).collect(Collectors.toList());
        //переключение на 2 окно
        driver.switchTo().window(otherWindows.stream().findFirst().get());
        // В поле «ФИО» ввести фамилию физического лица из предусловия.
        createAppealPage.setSearchFio(fio);
        // В поле «Любой E-mail» ввести E-mail физического лица из предусловия.
        createAppealPage.setSearchEmail(email);
        // Нажать на кнопку «Найти»
        createAppealPage.clickButtonSearch();
        // Выбрать физическое лицо из формы просмотра списка физических лиц.
        createAppealPage.clickFindedFio();
        // переключение на 1 окно
        driver.switchTo().window(initialWindow);
        // В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
        createAppealPage.clickCategory();
        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
        createAppealPage.setSelectEmployeeFck(value);
        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
        createAppealPage.clickSubtype();
        // Выбрать из выпадающего списка значение «Приглашение».
        createAppealPage.setSelectSubtypeInvitation(invitationValue);
        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
        createAppealPage.clickSubject();
        // Выбрать из выпадающего списка значение – «Мероприятия».
        createAppealPage.setSelectSubjectEvents(invitation_eventsValue);
        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
        createAppealPage.clickSubSubject();
        // Выбрать из выпадающего списка выбрать значение «Другое».
        createAppealPage.setSelectSubSubject(invitation_events_otherValue);
        // Переключение на фрейм 3
        createAppealPage.switchFrame();
        // В поле «Описание» вручную ввести текст – «Текст описания
        createAppealPage.setTinymceValue(text);
        // переключение на Default iFrame
        createAppealPage.switchToDefaultFrame();
        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
        createAppealPage.clickConnect();
        // Из выпадающего списка выбрать «Email».
        createAppealPage.setConnectValue(emailValue);
        // Клик по дроп - «Email для связи»
        createAppealPage.clickEmailForCommunication();
        // В поле «Email для связи» выбрать Email из предусловия.
        createAppealPage.clickToValueEmailForCommunication();
        // Нажать на кнопку «Сохранить и выйти».
        createAppealPage.clickSaveAndExit();

        //Asserts
        // Проверить статус обращения.
        Assertions.assertTrue(createAppealPage.getDontAssigned().getText().contains("Не назначено"), "В поле «Статус» не указано значение «Не назначено»");
        // В поле «Состояние» указано значение «Открыто»
        Assertions.assertTrue(createAppealPage.getConditionInput().getText().contains("Открыто"), "В поле «Состояние» не указано значение «Открыто»");


    }
}
