
import db_fck.DbConnection;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.jdbc.core.JdbcTemplate;

import java.net.MalformedURLException;
import java.time.Duration;

@Slf4j
public class TestBase {

    protected WebDriverWait wait;
    protected WebDriver driver;
    protected JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() throws MalformedURLException {
        //localTo
        WebDriverManager.chromedriver().setup();

        //local
//        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        //docker
//        WebDriverManager wdm = WebDriverManager.chromedriver().browserInDocker()
//                .enableVnc().enableRecording();
//        driver = wdm.create();

        //remote
//        driver = new RemoteWebDriver(new URL("http://100.64.13.139:5555/wd/hub"), capabilities);
//        driver = WebDriverManager.chromedriver().browserInDocker().remoteAddress("http://100.64.13.139:5555/wd/hub").create();

        // Selenium
        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless=new");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("window-size=1920,1080");

        driver = new ChromeDriver(options);

        DesiredCapabilities capabilities = new DesiredCapabilities();
        options.addArguments("incognito");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);


        //Selenoid
//        capabilities.setCapability("browserVersion", "112.0");
//        capabilities.setCapability("selenoid:options", new HashMap<String, Object>() {{
//            /* How to add test badge */
//            put("name", "Test badge...");
//            /* How to set session timeout */
//            put("sessionTimeout", "15m");
//            /* How to set timezone */
//            put("env", new ArrayList<String>() {{
//                add("TZ=UTC");
//            }});
//            /* How to add "trash" button */
//            put("labels", new HashMap<String, Object>() {{
//                put("manual", "true");
//            }});
//            /* How to enable video recording */
//            put("enableVideo", true);
//            /* VNC */
//            put("enableVNC", true);
//        }});
//        driver = new RemoteWebDriver(new URL("http://100.64.13.139:4444/wd/hub"), capabilities);
//

        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        // Подключение к БД
        DbConnection dbConnection = new DbConnection();
        jdbcTemplate = dbConnection.jdbcTemplate();

    }

    @AfterEach
    public void tearDown(){
        driver.quit();
    }


}
