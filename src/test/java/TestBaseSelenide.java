import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static com.codeborne.selenide.Selenide.*;

public class TestBaseSelenide {
    @BeforeEach
    public void setUp() {

        //localTo
        WebDriverManager.chromedriver().setup();

        //local
//        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        // Selenide
        com.codeborne.selenide.Configuration.browser = "chrome";
        com.codeborne.selenide.Configuration.driverManagerEnabled = true;
        com.codeborne.selenide.Configuration.browserSize = "1920x1080";
        Configuration.headless = false;
    }

    @AfterEach
    public void tearDown(){
        closeWebDriver();
    }
}
