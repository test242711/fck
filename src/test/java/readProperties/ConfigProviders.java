package readProperties;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public interface ConfigProviders {
    Config config = readConfig();

    static Config readConfig() {
        return ConfigFactory.load("application.conf");
    }

    String URL = readConfig().getString("url");
    String LOGIN = readConfig().getString("userParams.login");
    String PASSPORT = readConfig().getString("userParams.passport");
    // remoteDB
    String HOST_REMOTE = readConfig().getString("remoteDataBase.host");
    String USER_LOGIN = readConfig().getString("remoteDataBase.userName");
    String PASSPORT_DB = readConfig().getString("remoteDataBase.passport");
    // localDB
    String HOST_LOCAL = readConfig().getString("localDataBase.host");
    String USER_DB_LOGIN = readConfig().getString("localDataBase.userName");
    String PASSPORT_DB_LOCAL = readConfig().getString("localDataBase.passport");

}
