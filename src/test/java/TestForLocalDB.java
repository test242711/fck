import db_fck.DbConnection;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

public class TestForLocalDB {
    @Test
    public void localDB() {
        DbConnection dbConnection = new DbConnection();
        final JdbcTemplate jdbcTemplate = dbConnection.jdbcTemplateForLocal();

        // queryForMap — получить строку
        Map<String, Object> result = jdbcTemplate.queryForMap("SELECT * FROM test_base.courses WHERE teacher_id = 1;");

        // queryForList — получить столбец
        List<String> result2 = jdbcTemplate.queryForList("SELECT name FROM test_base.courses WHERE type = 'PROGRAMMING'",String.class);

        System.out.println(result2);
    }
}
