import com.microsoft.playwright.Page;
import db_fck.TestDB_TK9;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.*;
import pages.PW.AuthorizationPagePW;
import pages.PW.ContactFaceFormPW;
import pages.PW.NavigationPanelAllPagePW;
import pages.PW.PhysicalPersonPW;
import readProperties.ConfigProviders;

import java.sql.Date;

@DisplayName("ТК_А9 – Редактирование обращения")
public class EditingRequest_TK9 extends TestBasePW {
    /**
     * * Тест на Playwright
     */
    MainPagePW mainPagePW;
    AuthorizationPagePW authorizationPagePW;
    NavigationPanelAllPagePW navigationPanelAllPagePW;
    PhysicalPersonPW physicalPersonPW;
    AppealPage appealPage;
    ContactFaceFormPW contactFaceFormPW;

    @Test
    @DisplayName("ТК_А9 – Редактирование обращения")
    public void editRequest() {
        // arrange
        // Открыть тестовый стенд http://fckproject.itfbgroup.ru/fcktest_001/.
        mainPagePW = new MainPagePW(page);
        mainPagePW.setUrl(ConfigProviders.URL);
        // Авторизация
        authorizationPagePW = new AuthorizationPagePW(page);
        // Выбрать закладку модулей «Все».
        mainPagePW.hoverToAll();
        // В выпадающем списке выбрать пункт «Физические лица» («Contacts»).
        navigationPanelAllPagePW = new NavigationPanelAllPagePW(page);
        navigationPanelAllPagePW.clickToPhysicalPerson();
        // В меню модуля выбрать пункт «Создать новое физическое лицо».
        physicalPersonPW = new PhysicalPersonPW(page);
        physicalPersonPW.clickCreatePhysic();
        // В разделе «Основная информация» заполнить поле «Фамилия» -
        // рандомный набор букв на кириллице от 3 до 12 символов.
        physicalPersonPW.setRandomLastName();
        // В разделе «Основная информация» заполнить поле «Имя» -
        // рандомный набор букв на кириллице от 3 до 12 символов.
        physicalPersonPW.setName();
        // В разделе «Основная информация» заполнить поле «Отчество» -
        // рандомный набор букв на кириллице от 3 до 12 символов.
        physicalPersonPW.setSecondName();
        // В разделе «Основная информация» заполнить поле «Категория контакта» -
        // нажать на поле и выбрать из выпадающего списка значение «Сотрудник предприятия»
        physicalPersonPW.setCategoryOnDrop();
        // В разделе «Основная информация» заполнить поле «Предприятие» - нажать на кнопку со стрелкой справа от поля.
        Page newWindow = context.waitForPage(() -> physicalPersonPW.clickCompanyArrow());
        // Заполнить поле «ИНН» - 6321277661.
        physicalPersonPW = new PhysicalPersonPW(newWindow);
        physicalPersonPW.setInn();
        // Нажать на кнопку «Найти».
        physicalPersonPW.clickButton();
        // Выбрать найденное предприятие.
        physicalPersonPW.clickCompany();
        // В разделе «Основная информация» заполнить поле «Должность» - менеджер.
        physicalPersonPW = new PhysicalPersonPW(page);
        physicalPersonPW.fillPostManager();
        // В разделе «Контактные данные» заполнить по маске ХХХХХХХХХХ поле «Телефон»
        physicalPersonPW.fillPhone();
        // В подразделе «Контактные данные» заполнить поле «E-mail» по маске  XXX@XXX.XX.
        physicalPersonPW.fillEmail();
        // Под Формой создания физического лица нажать на кнопку «Сохранить и выйти».
        physicalPersonPW.saveAndExit();
        // На основной закладке выбрать закладку модулей «Все».
        mainPagePW.hoverToAll();
        // Из выпадающего списка выбрать пункт «Обращения».
        appealPage = new AppealPage(page);
        appealPage.clickToAppeal();
        // В меню модуля выбрать пункт «Создать обращение».
        appealPage.createToAppeal();
        // В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.
        Page newWindow2 = context.waitForPage(() -> appealPage.clickContactFaceArrow());
        // В поле «ФИО» ввести %<Фамилия>%, где <Фамилия>=Фамилия, созданного физического лица.
        contactFaceFormPW = new ContactFaceFormPW(newWindow2);
        contactFaceFormPW.setFIO();
        // Нажать на кнопку «Найти»
        contactFaceFormPW.saveAndExit();
        // Выбрать физическое лицо, созданное ранее, из формы просмотра списка физических лиц.
        contactFaceFormPW.getPhisFace();
        // В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
        appealPage = new AppealPage(page);
        appealPage.clickCategory();
        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
        appealPage.setCategoryFckEmployer();
        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
        appealPage.clickPodtip();
        // Выбрать из выпадающего списка значение «Консультация».
        appealPage.setConsultation();
        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
        appealPage.clickTema();
        // Выбрать из выпадающего списка значение «Работа платформы».
        appealPage.setWorkPlatform();
        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
        appealPage.clickSubTem();
        // Выбрать из выпадающего списка значение «Регистрация на портале».
        appealPage.setRegistrationOnPortal();
        // переключение на iFrame
        // В поле «Описание» вручную ввести текст – «Текст описания»
        appealPage.fillText();
        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
        appealPage.setDesireMethodOfCommunication();
        // Из выпадающего списка выбрать «Email».
        appealPage.setEmail();
        // В поле «Email для связи» указать Email из предусловия.
        appealPage.selectEmail();
        // Нажать на кнопку «Сохранить и выйти».
        appealPage.clickSaveAndExit();
        // Получение данных из БД
        Integer intervalProcessing = jdbcTemplate.queryForObject(
                "SELECT processing_time FROM db_auto.ods_time_management ORDER BY processing_time DESC LIMIT 1", Integer.class);
        Date processingDeadline = jdbcTemplate.queryForObject(
                "SELECT processing_deadline FROM db_auto.cases ORDER BY processing_deadline DESC LIMIT 1", Date.class);
        // Проверить заполнение поля «Крайний срок обработки».
        TestDB_TK9 testDB_tk9 = new TestDB_TK9();
        testDB_tk9.assertProcessingDeadlineField(intervalProcessing,processingDeadline);
        // Проверить заполнение поля «Вес обращения».
        Integer weightAppeal = jdbcTemplate.queryForObject(
                "SELECT weight FROM db_auto.cases ORDER BY weight LIMIT 1", Integer.class); // «Вес обращения»



    }

}
