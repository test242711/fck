
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.StaleElementReferenceException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import pages.*;

import java.util.stream.Collectors;

@Slf4j
@DisplayName("ТК_А6 - Назначение ответственного по обращению")
public class AppointmentOfThePersonResponsibleForTheAppeal_TK6 extends TestBase {

    @Test()
    @DisplayName("ТК_А6 - Назначение ответственного по обращению")
    public void appointmentPersonResponsibleForTheAppeal() {
        // arrange
        String fio = "Ласточкин Борис Васильевич";
        String fckEmployee = "fck";
        String consultValue = "consult";
        String subjectValueConsultFck = "consult_fck";
        String consultFckOtherValue = "consult_fck_other";
        String text = "Текст описания";
        String emailValue = "email";
        String sqlSelectCommand = "SELECT securitygroup_id FROM `securitygroups_users` " +
                "WHERE `user_id` LIKE '67d2a2e2-5f6e-423c-6894-5f74970445f3' " +
                "AND for_filter='1'";
        String idAssert = "3947f96e-9642-954f-18e0-5f74540d8ea9";
        String lastName = "Ласточкин";
        String firstName = "Борис";

        // Открыть тестовый стенд
        MainPage mainPage = new MainPage(this.driver, this.wait);
        mainPage.openPage()
                .authorize()
                .toContacts();
//        // авторизация
//        AuthorizationPage authorizationPage = new AuthorizationPage(this.driver, this.wait);
//        authorizationPage.authorize();
//
//        // Открыть модуль «Администрирование».
//        mainPage.navigationPanelAllPage.clickToAccountIconOnTheRight();
//        mainPage.navigationPanelAllPage.clickToAdminMenu();
        // Раздел Администрирование
        AdministrationPage administrationPage = new AdministrationPage(this.driver, this.wait);
        // В разделе меню «Система» выбрать «Настройка конфигурации».
        administrationPage.clickConfigSetting();
        // Внизу формы просмотра кликнуть на «Просмотр журнала».
        administrationPage.clickWatchLogs();
        // сохраняем текущее окно
        var initialWindow = driver.getWindowHandle();
        // сохраняем все окна
        var allWindows = driver.getWindowHandles();
        // определяем открытое окно
        var otherWindows = allWindows.stream().filter(x -> !x.equals(initialWindow)).collect(Collectors.toList());
        //переключение на 2 окно
        driver.switchTo().window(otherWindows.stream().findFirst().get());
        // Нажать на кнопку «Установить контрольную точку».
        administrationPage.clickSetControlPoint();
        // переключение на 1 окно
        driver.switchTo().window(initialWindow);
        // В другой вкладке браузера открыть тестовый стенд http://fckproject.itfbgroup.ru/fcktest_001/ и выбрать закладку модулей «Все».
//        administrationPage.navigationPanelAllPage.allMenu();
//        // Из выпадающего списка выбрать пункт «Обращения».
//        administrationPage.navigationPanelAllPage.clickToAppealsMenu();
        // Переход в меню «Создать обращение».
        CreateAppealPage createAppealPage = new CreateAppealPage(this.driver, this.wait);
        // В меню модуля выбрать пункт «Создать обращение».
        createAppealPage.clickCreateAppeal();
        // В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.
        createAppealPage.clickArrowOnTheRightOfInputContactFace();
        // получить все окна
        var getAllWindows = driver.getWindowHandles();
        // получить 3 окно по индексу
        var getToNewWindow = getAllWindows.toArray()[2];
        // переключение на 3 окно (Поиск предприятий)
        driver.switchTo().window(String.valueOf(getToNewWindow));
        // В поле «ФИО» ввести Фамилия физического лица из предусловия.
        createAppealPage.setFindingName(fio);
        // Нажать на кнопку «Найти»
        createAppealPage.clickSearchButton();
        // Выбрать физическое лицо, созданное ранее, из формы просмотра списка физических лиц.
        createAppealPage.clickFirstFindName();
        // В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
        createAppealPage.clickCategorySelector();
        // переключение на 1 вкладку
        driver.switchTo().window(String.valueOf(initialWindow));
        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
        createAppealPage.setCategoryByValue(fckEmployee);
        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
        createAppealPage.clickSubtypeSelector();
        // Выбрать из выпадающего списка значение «Консультация».
        createAppealPage.setConsultValue(consultValue);
        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
        createAppealPage.clickSubjectSelector();
        // Выбрать из выпадающего списка значение «Работа ФЦК».
        createAppealPage.setSelectSubjectEvents(subjectValueConsultFck);
        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
        createAppealPage.clickSubSubject();
        // Выбрать из выпадающего списка выбрать значение «Другое».
        createAppealPage.setSelectSubSubject(consultFckOtherValue);
        // Переключение на фрейм 3
        createAppealPage.switchFrame();
        // В поле «Описание» вручную ввести текст – «Текст описания
        createAppealPage.setTinymceValue(text);
        // переключение на Default iFrame
        createAppealPage.switchToDefaultFrame();
        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
        createAppealPage.clickConnectAnon();
        // Из выпадающего списка выбрать «Email».
        createAppealPage.setConnectValue(emailValue);
        // В поле «Email для связи» выбрать из выпадающего списка сохраненный Email.
        createAppealPage.setEmailForConnect();
        // Нажать на кнопку «Сохранить и выйти».
        createAppealPage.clickSaveAndExit();
        // В форме просмотра обращения нажать на кнопку «Назначить ответственного».
        createAppealPage.clickAppointAResponsible();
        // Перейти на сайт СУБД из предусловия.
        try {
            DBPage dbPage = new DBPage(this.driver,  this.wait);
            dbPage.open();
            // переключение на 1 окно
            driver.switchTo().window(initialWindow);
            // авторизация в СУБД
            dbPage.authorize();
            // Нажать на кнопку "Вперед"
            dbPage.clickButtonIntoGo();
            // На боковой панели выбрать БД «db_auto»
            dbPage.clickDbAuto();
            // Ожидание выбора БД
            dbPage.waitSelectDb();
            // На основной панели нажать на кнопку «SQL».
            dbPage.clickSqlButton();
            // В поле ввода запроса ввести запрос:
            //SELECT securitygroup_id FROM `securitygroups_users`
            // WHERE `user_id` LIKE '67d2a2e2-5f6e-423c-6894-5f74970445f3'
            // AND for_filter='1'
            dbPage.writeSqlCommand(sqlSelectCommand);
            // Нажать на кнопку "Вперед"
            dbPage.clickButtonSubmitQuery();
            // Проверить в окне поиска сотрудников, доступных для назначения,
            // в адресной строке id группы
            // (%ods_securitygroup_id_advanced=3947f96e-9642-954f-18e0-5f74540d8ea9%)
            Assertions.assertEquals(idAssert, dbPage.getResultIdSqlResponse(),"Id не соответствует искомому");
        }
        catch (StaleElementReferenceException staleElementNotFound) {
            log.info("Кнопка clickDbAuto() На боковой панели выбрать БД «db_auto» периодически не срабатывает");
        }
        try {
            // проверка в самой БД
            String getSecurityGroupFromDB = jdbcTemplate.queryForObject(sqlSelectCommand, String.class);
            Assertions.assertEquals(idAssert, getSecurityGroupFromDB,"Id не соответствует искомому");
        }
        catch (CannotGetJdbcConnectionException cannotGetJdbcConnectionException) {
            log.info("Нет подключения к RemoteDB");
        }
        finally {
            // получить все окна
            var getAllWindows2 = driver.getWindowHandles();
            // получить 3 окно по индексу
            var getToNewWindow2 = getAllWindows2.toArray()[2];
            // переключение на 3 окно (Поиск предприятий)
            driver.switchTo().window(String.valueOf(getToNewWindow2));
            // Окно "Поиск пользователей"
            ClientSearchWindowPage clientSearchWindowPage = new ClientSearchWindowPage(this.driver, this.wait);
            // В поле «Фамилия» ввести фамилию сотрудника, на которого необходимо назначить обращение – фамилия сотрудника из предусловия.
            clientSearchWindowPage.setLastName(lastName);
            // В поле «Имя» ввести имя сотрудника, на которого необходимо назначить обращение – имя сотрудника из предусловия
            clientSearchWindowPage.setFirstName(firstName);
            // Нажать на кнопку «Найти».
            clientSearchWindowPage.clickSButtonSearchForm();
        }
    }
}
